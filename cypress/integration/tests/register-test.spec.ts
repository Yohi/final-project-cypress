/// <reference types= "cypress"/>

import { SignUpPage } from '../../src/pages/signup-page';
import { HumbleUsers } from '../../src/HumbleUsers';
import { ActivationPage } from '../../src/pages/activation-page';
import { activationPageLocators } from '../../src/pages/activation-page-locators';

Cypress.on('uncaught:exception', (err) => {
    return false
})

describe('Registeration Test', () => {
    const signUpPage = new SignUpPage();

    it('Create a user', () => {
        signUpPage.createAUser();
    });

    it('Go to signup page', () => {
        signUpPage.goToSignUpPage();
        
    });

    it('Fill email', () => {
        cy.fixture('RegisterUser.json').then((theData) => {
            const obj:HumbleUsers = { ...theData};
            signUpPage.fillEmail(obj.email);
        })
    });

    it('Fill password', () => {
        cy.fixture('RegisterUser.json').then((theData) => {
            const obj:HumbleUsers = { ...theData};
            signUpPage.fillPassword(obj.password);
        })
    });

    it('Uncheck notification box', () => {
        signUpPage.uncheckSubscription();
    });

    it('Sign up', () => {
        signUpPage.clickSignUp();
    })

    const activationPage = new ActivationPage();
    it('Verify sign up', () => {
        activationPage.verifyActivisionTitle();
        
        cy.fixture('RegisterUser.json').then((theData) => {
            const obj:HumbleUsers = { ...theData};
            activationPage.verifyActivisionEmail(obj.email);
        })
    })
});