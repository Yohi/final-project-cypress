/// <reference types= "cypress"/>

import { CategoryList } from '../../src/CategoryList';
import { HomePage } from '../../src/pages/home-page';
import { homePageLocators } from '../../src/pages/home-page-locators';

Cypress.on('uncaught:exception', (err) => {
    return false
})

describe('Validate About Menu', () => {
    const homePage = new HomePage();
    it('Navigate to site', () => {
        homePage.goToPage();
    })

    // Checking if an element that takes a lot of time to appear, has appeared.
    it('Validating navigation bar has loaded', () => {
        homePage.validateNavbarVisibility();
    });

    it('Click on About Menu', () => {
        homePage.clickOnAbout();
    })
    
    // Checking elements that appear after an action.
    it('Validating About Menu', () => {
        cy.fixture('AboutMenuList.json').then((theData) => {
            const obj:CategoryList = { ...theData.listData};
            homePage.validateAboutList(obj);
        })
    });
})