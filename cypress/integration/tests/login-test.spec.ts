/// <reference types= "cypress"/>

import { SignInPage } from '../../src/pages/signin-page';
import { signinPageLocators } from '../../src/pages/signin-page-locators';
import { HumbleUsers } from '../../src/HumbleUsers';
import { ActivationPage } from '../../src/pages/activation-page';

Cypress.on('uncaught:exception', (err) => {
    return false
})
// Check if this works with existing user.
describe('Logging in test', () => {
    const signInPage = new SignInPage();

    it('Go to login page', () => {
        signInPage.goToSignInPage();
    });

    it('Fill email', () => {
        cy.fixture('LoginUser.json').then((theData) => {
            const obj:HumbleUsers = { ...theData};
            signInPage.fillEmail(obj.email);
        })
    });

    it('Fill password', () => {
        cy.fixture('LoginUser.json').then((theData) => {
            const obj:HumbleUsers = { ...theData};
            signInPage.fillPassword(obj.password);
        })
    });

    it('Log into the account', () => {
        signInPage.loginTheAccount();
    });

    const activationPage = new ActivationPage();
    it('Verify log in', () => {
        signInPage.enterUserMenu();
        signInPage.validateLogoutButton();
    })
});
