interface activationPageLocators {
    userEmail:string;
    pageTitle:string;
}
export const activationPageLocators = {
    "userEmail":"#id_email",
    "pageTitle": ".page_title"
}