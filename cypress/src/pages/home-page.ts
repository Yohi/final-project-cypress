/// <reference types= "cypress"/>

import { BasePage } from "./base-page";
import { homePageLocators } from './home-page-locators';
import { info } from "console";
import { CategoryList } from "../CategoryList";

export class HomePage extends BasePage {
    chosenProductIndex: any;
    
    constructor() {
        super();
        this.chosenProductIndex
    }

    clickOnAbout() {
        cy.get(homePageLocators.aboutButton).click();
    }

    validateAboutList(obj:CategoryList) {
        for (let i = 0; i < obj.CategoryList.length; i++) {
            cy.get(homePageLocators.aboutMenuItems).eq(i).should
            (($NBATabList) => {
                expect($NBATabList.text()).contains(obj.CategoryList[i]);
            });
        }
    }

    validateNavbarVisibility() {
        cy.get(homePageLocators.navigationBar).should('be.visible');
    }
}