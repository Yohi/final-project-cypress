export interface signupPageLocators {
    emailInput:string;
    passwordInput:string;
    subscribeCheckbox:string;
    signUpButton:string;
}

export const signupPageLocators = {
    "emailInput": "[name = 'email']",
    "passwordInput": "[name = 'password']",
    "subscribeCheckbox": "#subscribe-checkbox",
    "signUpButton": "[type = 'submit']"
}