/// <reference types= "cypress"/>

export class BasePage {
    private fullUrl: string;
    
    constructor(pageUrlPath: string = "") {
        this.fullUrl = Cypress.config().baseUrl + pageUrlPath;
    }
   
    goToPage() {
           cy.visit(this.fullUrl);
    }

    goToSignUpPage() {
        cy.visit(this.fullUrl + "signup?hmb_source=navbar&goto=%2F");
    }

    goToSignInPage() {
        cy.visit("https://www.saucedemo.com/");
    }
}