/// <reference types= "cypress"/>

import { BasePage } from "./base-page";
import { signinPageLocators } from './signin-page-locators';
import { info } from "console";

export class SignInPage extends BasePage {
    constructor() {
        super();
    }

    fillEmail(user:string) {
        cy.get(signinPageLocators.loginUser).type(user);
    }

    fillPassword(password:string) {
        cy.get(signinPageLocators.loginPassword).type(password);
    }

    loginTheAccount() {
        cy.get(signinPageLocators.loginButton).click();
    }

    enterUserMenu() {
        cy.get(signinPageLocators.validateLoginMenu).click();
    }

    validateLogoutButton() {
        cy.get(signinPageLocators.validateLoginLogoutButton).should('be.visible');
    }
}