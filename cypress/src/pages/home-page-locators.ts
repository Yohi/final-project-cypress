interface homePageLocators {
    signInButton:string;
    signUpButton:string;
    aboutButton:string;
    aboutMenuItems:string;
    navigationBar:string;
}

export const homePageLocators = {
    "signInButton": ".js-account-login",
    "signUpButton": ".js-create-account",
    "aboutButton": ".js-about-item-dropdown-toggle",
    "aboutMenuItems": ".about-items a",
    "navigationBar": ".navbar-content"
}