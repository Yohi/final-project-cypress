/// <reference types= "cypress"/>

import { BasePage } from "./base-page";
import { signupPageLocators } from './signup-page-locators';
import { info } from "console";

export class SignUpPage extends BasePage {
    constructor() {
        super();
    }

    createAUser() {
        let filePath = 'cypress/fixtures/RegisterUser.json'
        let user = {
            email: Math.random().toString(36).substr(2, 6) + "@gmail.com",
            password: Math.random().toString(36).substr(2, 9)
        };

        cy.writeFile(filePath, user);
    }

    fillEmail(email:string) {
        cy.get(signupPageLocators.emailInput).type(email);
    }

    fillPassword(password:string) {
        cy.get(signupPageLocators.passwordInput).type(password);
    }

    uncheckSubscription() {
        cy.get(signupPageLocators.subscribeCheckbox).uncheck();
        cy.get(signupPageLocators.subscribeCheckbox).should('not.be.checked');
    }

    clickSignUp() {
        cy.get(signupPageLocators.signUpButton).click();
    }
}