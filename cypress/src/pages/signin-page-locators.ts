export interface signinPageLocators {
    loginEmail:string;
    loginPassword:string;
    loginButton:string;
    validateLoginMenu:string;
    validateLoginLogoutButton:string;
}

export const signinPageLocators = {
    "loginUser": "#user-name",
    "loginPassword": "#password",
    "loginButton": "#login-button",
    "validateLoginMenu": "#react-burger-menu-btn",
    "validateLoginLogoutButton": "#logout_sidebar_link"
}