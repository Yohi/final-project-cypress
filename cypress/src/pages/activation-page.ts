/// <reference types= "cypress"/>

import { BasePage } from "./base-page";
import { activationPageLocators } from './activation-page-locators';
import { info } from "console";

export class ActivationPage extends BasePage {
    activisionTitle:string;
    
    constructor() {
        super();
        this.activisionTitle = "Activate your account";
    }

    verifyActivisionTitle() {
        // Time out is vital because the page takes time to load.
        cy.get(activationPageLocators.pageTitle, {timeout: 10000}).should('be.visible');
        cy.get(activationPageLocators.pageTitle).invoke('text').should('contain', this.activisionTitle);
    }

    verifyActivisionEmail(email:string) {
        cy.get(activationPageLocators.userEmail).should('be.visible');
        cy.get(activationPageLocators.userEmail).should('have.value', email);
    }
}