/// <reference types= "cypress"/>
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { SignInPage } from '../../src/pages/signin-page';
import { ActivationPage } from '../../src/pages/activation-page';
Cypress.on('uncaught:exception', function (err) {
    return false;
});
// Check if this works with existing user.
describe('Logging in test', function () {
    var signInPage = new SignInPage();
    it('Go to login page', function () {
        signInPage.goToSignInPage();
    });
    it('Fill email', function () {
        cy.fixture('LoginUser.json').then(function (theData) {
            var obj = __assign({}, theData);
            signInPage.fillEmail(obj.email);
        });
    });
    it('Fill password', function () {
        cy.fixture('LoginUser.json').then(function (theData) {
            var obj = __assign({}, theData);
            signInPage.fillPassword(obj.password);
        });
    });
    it('Log into the account', function () {
        signInPage.loginTheAccount();
    });
    var activationPage = new ActivationPage();
    it('Verify log in', function () {
        signInPage.enterUserMenu();
        signInPage.validateLogoutButton();
    });
});
