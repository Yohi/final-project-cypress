/// <reference types= "cypress"/>
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { SignUpPage } from '../../src/pages/signup-page';
import { ActivationPage } from '../../src/pages/activation-page';
Cypress.on('uncaught:exception', function (err) {
    return false;
});
describe('Registeration Test', function () {
    var signUpPage = new SignUpPage();
    it('Create a user', function () {
        signUpPage.createAUser();
    });
    it('Go to signup page', function () {
        signUpPage.goToSignUpPage();
    });
    it('Fill email', function () {
        cy.fixture('RegisterUser.json').then(function (theData) {
            var obj = __assign({}, theData);
            signUpPage.fillEmail(obj.email);
        });
    });
    it('Fill password', function () {
        cy.fixture('RegisterUser.json').then(function (theData) {
            var obj = __assign({}, theData);
            signUpPage.fillPassword(obj.password);
        });
    });
    it('Uncheck notification box', function () {
        signUpPage.uncheckSubscription();
    });
    it('Sign up', function () {
        signUpPage.clickSignUp();
    });
    var activationPage = new ActivationPage();
    it('Verify sign up', function () {
        // Need to fix
        activationPage.verifyActivisionTitle();
        cy.fixture('RegisterUser.json').then(function (theData) {
            var obj = __assign({}, theData);
            activationPage.verifyActivisionEmail(obj.email);
        });
    });
});
