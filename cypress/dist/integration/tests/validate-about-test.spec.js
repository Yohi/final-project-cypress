/// <reference types= "cypress"/>
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { HomePage } from '../../src/pages/home-page';
Cypress.on('uncaught:exception', function (err) {
    return false;
});
describe('Validate About Menu', function () {
    var homePage = new HomePage();
    it('Navigate to site', function () {
        homePage.goToPage();
    });
    // Checking if an element that takes a lot of time to appear, has appeared.
    it('Validating navigation bar has loaded', function () {
        homePage.validateNavbarVisibility();
    });
    it('Click on About Menu', function () {
        homePage.clickOnAbout();
    });
    // Checking elements that appear after an action.
    it('Validating About Menu', function () {
        cy.fixture('AboutMenuList.json').then(function (theData) {
            var obj = __assign({}, theData.listData);
            homePage.validateAboutList(obj);
        });
    });
});
