/// <reference types= "cypress"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BasePage } from "./base-page";
import { signinPageLocators } from './signin-page-locators';
var SignInPage = /** @class */ (function (_super) {
    __extends(SignInPage, _super);
    function SignInPage() {
        return _super.call(this) || this;
    }
    SignInPage.prototype.fillEmail = function (user) {
        cy.get(signinPageLocators.loginUser).type(user);
    };
    SignInPage.prototype.fillPassword = function (password) {
        cy.get(signinPageLocators.loginPassword).type(password);
    };
    SignInPage.prototype.loginTheAccount = function () {
        cy.get(signinPageLocators.loginButton).click();
    };
    SignInPage.prototype.enterUserMenu = function () {
        cy.get(signinPageLocators.validateLoginMenu).click();
    };
    SignInPage.prototype.validateLogoutButton = function () {
        cy.wait(signinPageLocators.validateLoginLogoutButton).should('be.visible');
    };
    return SignInPage;
}(BasePage));
export { SignInPage };
