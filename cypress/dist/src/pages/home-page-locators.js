export var homePageLocators = {
    "signInButton": ".js-account-login",
    "signUpButton": ".js-create-account",
    "aboutButton": ".js-about-item-dropdown-toggle",
    "aboutMenuItems": ".about-items a",
    "navigationBar": ".navbar-content"
};
