export var signupPageLocators = {
    "emailInput": "[name = 'email']",
    "passwordInput": "[name = 'password']",
    "subscribeCheckbox": "#subscribe-checkbox",
    "signUpButton": "[type = 'submit']"
};
