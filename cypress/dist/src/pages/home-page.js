/// <reference types= "cypress"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BasePage } from "./base-page";
import { homePageLocators } from './home-page-locators';
var HomePage = /** @class */ (function (_super) {
    __extends(HomePage, _super);
    function HomePage() {
        var _this = _super.call(this) || this;
        _this.chosenProductIndex;
        return _this;
    }
    HomePage.prototype.clickOnAbout = function () {
        cy.get(homePageLocators.aboutButton).click();
    };
    HomePage.prototype.validateAboutList = function (obj) {
        var _loop_1 = function (i) {
            cy.get(homePageLocators.aboutMenuItems).eq(i).should(function ($NBATabList) {
                expect($NBATabList.text()).contains(obj.CategoryList[i]);
            });
        };
        for (var i = 0; i < obj.CategoryList.length; i++) {
            _loop_1(i);
        }
    };
    HomePage.prototype.validateNavbarVisibility = function () {
        cy.get(homePageLocators.navigationBar).should('be.visible');
    };
    return HomePage;
}(BasePage));
export { HomePage };
