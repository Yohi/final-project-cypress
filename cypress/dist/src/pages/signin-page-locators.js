export var signinPageLocators = {
    "loginUser": "#user-name",
    "loginPassword": "#password",
    "loginButton": "#login-button",
    "validateLoginMenu": "#react-burger-menu-btn",
    "validateLoginLogoutButton": "#logout_sidebar_link"
};
