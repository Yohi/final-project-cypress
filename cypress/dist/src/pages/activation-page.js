/// <reference types= "cypress"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BasePage } from "./base-page";
import { activationPageLocators } from './activation-page-locators';
var ActivationPage = /** @class */ (function (_super) {
    __extends(ActivationPage, _super);
    function ActivationPage() {
        var _this = _super.call(this) || this;
        _this.activisionTitle = "Activate your account";
        return _this;
    }
    ActivationPage.prototype.verifyActivisionTitle = function () {
        cy.get(activationPageLocators.pageTitle).should('be.visible');
        cy.get(activationPageLocators.pageTitle).invoke('text').should('contain', this.activisionTitle);
    };
    ActivationPage.prototype.verifyActivisionEmail = function (email) {
        cy.get(activationPageLocators.userEmail).should('be.visible');
        cy.get(activationPageLocators.userEmail).should('have.value', email);
    };
    return ActivationPage;
}(BasePage));
export { ActivationPage };
