/// <reference types= "cypress"/>
var BasePage = /** @class */ (function () {
    function BasePage(pageUrlPath) {
        if (pageUrlPath === void 0) { pageUrlPath = ""; }
        this.fullUrl = Cypress.config().baseUrl + pageUrlPath;
    }
    BasePage.prototype.goToPage = function () {
        cy.visit(this.fullUrl);
    };
    BasePage.prototype.goToSignUpPage = function () {
        cy.visit(this.fullUrl + "signup?hmb_source=navbar&goto=%2F");
    };
    BasePage.prototype.goToSignInPage = function () {
        cy.visit("https://www.saucedemo.com/");
    };
    return BasePage;
}());
export { BasePage };
