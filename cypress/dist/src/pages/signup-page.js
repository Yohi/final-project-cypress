/// <reference types= "cypress"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BasePage } from "./base-page";
import { signupPageLocators } from './signup-page-locators';
var SignUpPage = /** @class */ (function (_super) {
    __extends(SignUpPage, _super);
    function SignUpPage() {
        return _super.call(this) || this;
    }
    SignUpPage.prototype.createAUser = function () {
        var filePath = 'cypress/fixtures/RegisterUser.json';
        var user = {
            email: Math.random().toString(36).substr(2, 6) + "@gmail.com",
            password: Math.random().toString(36).substr(2, 9)
        };
        cy.writeFile(filePath, user);
        //     cy.readFile(filePath).then((List) => {
        //         List.push({ user: user })
        //         // write the merged array
        //         cy.writeFile(filePath, List)
        //       })
        //    // cy.writeFile(filePath, user, { flag: 'a+' });
    };
    SignUpPage.prototype.fillEmail = function (email) {
        cy.get(signupPageLocators.emailInput).type(email);
    };
    SignUpPage.prototype.fillPassword = function (password) {
        cy.get(signupPageLocators.passwordInput).type(password);
    };
    SignUpPage.prototype.uncheckSubscription = function () {
        cy.get(signupPageLocators.subscribeCheckbox).uncheck();
        cy.get(signupPageLocators.subscribeCheckbox).should('not.be.checked');
    };
    SignUpPage.prototype.clickSignUp = function () {
        cy.get(signupPageLocators.signUpButton).click();
    };
    return SignUpPage;
}(BasePage));
export { SignUpPage };
